<!DOCTYPE html>
<html lang="es">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HelpMeToExplode</title>
</head>

<?php
if($_POST){
    $extraido = [];
    $bullet= [];    
    $faltan = [];
    $palabras_titulo = [];
    $palabras_desc = [];
    $palabras = nl2br($_POST['keys']);    
    $palabras = explode("<br />",$palabras);      
    $pal2 = implode(" ",$palabras);
    $palabras = explode(" ",$pal2);
    //$palabras = explode(' ', $_POST["keys"]);
    $bull = explode(' ',$_POST["bullet"]);      
    $titulo = explode(' ',$_POST["titulo"]);
    $descripcion = explode(' ',$_POST["desc"]);
    $errores = [];    
    // Keys
    foreach($palabras as $nom){
        /*if(strlen($nom)<=250){                            
            $extraido[] = trim(strtolower($nom));
        }
        else{
            $errores[] = "La palabra ".$nom." tiene más de 250 <br/>";
        }*/
        $texto = htmlentities($nom);
        $texto =  preg_replace('/\&(.)[^;]*;/', '\\1', $texto);       
        $texto = str_replace(',','',$texto);
        $texto = str_replace('(','',$texto);
        $texto = str_replace(')','',$texto);        
        if(strlen($texto)>1){
            $extraido[] = trim(strtolower($texto));        
        }
    }    
    

    // Título
    foreach($titulo as $ti){         
        $texto = htmlentities($ti);
        $texto =  preg_replace('/\&(.)[^;]*;/', '\\1', $texto);       
        $texto = str_replace(',','',$texto);
        $texto = str_replace('(','',$texto);
        $texto = str_replace(')','',$texto);        
        if(strlen($texto)>1){
            $palabras_titulo[] = trim(strtolower($texto));        
        }
    }    
    

    // Bullet
    foreach($bull as $nom){
        $texto = htmlentities($ti);
        $texto =  preg_replace('/\&(.)[^;]*;/', '\\1', $texto);       
        $texto = str_replace(',','',$texto);
        $texto = str_replace('(','',$texto);
        $texto = str_replace(')','',$texto);        
        if(strlen($texto)>1){              
            $bullet[] = trim(strtolower($nom));
        }        
    }    


    // Descripción
    foreach($descripcion as $desc){   
        $texto = htmlentities($desc);
        $texto =  preg_replace('/\&(.)[^;]*;/', '\\1', $texto);       
        $texto = str_replace(',','',$texto);
        $texto = str_replace('(','',$texto);
        $texto = str_replace(')','',$texto);        
        if(strlen($texto)>1){     
            $palabras_desc[] = trim(strtolower($desc));        
        }
    } 
    
    // Buscamos en título, bullet y descripción los que no están en la lista
  

    
    foreach($extraido as $bu){        
        if( (!in_array($bu,$bullet) and !in_array($bu,$palabras_titulo) and !in_array($bu,$palabras_desc))){
            if(in_array($bu,$faltan)){
                
            }
            else{
                $faltan[] = $bu;
            }
        }
    }    
        
}





?>
<body>
<div class="container">
    <div class="row">
    <div class="co-sm-4 offset-sm-4 col-xs-12">
    <h1>HelpMeToExplode</h1>
    </div>
    </div>
    
    
    <form action="#" method="post">
        <div class="form-group">
            <label for="keys">Lista</label>
            <textarea class="form-control" name="keys" id="keys" ><?php if(isset($_POST["keys"])){ echo $_POST["keys"];} ?></textarea>
        </div>
        <div class="form-group">
            <label for="titulo">Título</label>
            <textarea class="form-control" name="titulo" id="titulo" cols="30" rows="10"><?php if(isset($_POST["titulo"])){ echo $_POST["titulo"];} ?></textarea>
        </div>
        <div class="form-group">
            <label for="bullet">Bullet</label>
            <textarea class="form-control" name="bullet" id="bullet" cols="30" rows="10"><?php if(isset($_POST["bullet"])){ echo $_POST["bullet"];} ?></textarea>        
        </div>
        <div class="form-group">
            <label for="desc">Descripción</label>
            <textarea class="form-control" name="desc" id="desc" cols="30" rows="10"><?php if(isset($_POST["desc"])){ echo $_POST["desc"];} ?></textarea>
        </div>
        <div class="form-group">
            <label for="faltan">Faltan</label>
            <textarea class="form-control" name="faltan" id="faltan" cols="30" rows="10"><?php if(isset($faltan)){foreach($faltan as $fal){ echo utf8_decode($fal)."\n";}} ?></textarea>
        </div>
        <input class="mb-4 btn btn-primary" type="submit" value="Consultar">
    
    
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>